import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    lang: 'en',
  },
  mutations: {
    setLang(state, pyload) {
      state.lang = pyload;
    },
  },
  actions: {
  },
  modules: {
  },
});
