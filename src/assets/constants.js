export const LANGUAGE_LIST = [
  {
    code: 'ru',
    title: 'русский',
    id: 0,
  },
  {
    code: 'en',
    title: 'english',
    id: 1,
  },
  {
    code: 'tur',
    title: 'turkish',
    id: 2,
  },
];

export const BONUS_SECTION = {
  or: {
    en: 'or',
    tur: 'yada',
    ru: 'или',
  },
  title: {
    en: ['choose your', 'free', 'bonus'],
    ru: ['Выбери свой', 'бесплатный', 'бонус'],
    tur: ['', 'Ücretsiz', 'bonusunuzu seçin'],
  },
  airplane: {
    type: 'airplane',
    desc: {
      ru: '+3 дня премиум аккаунта',
      en: '+3 day premium account',
      tur: '+3 gün Premium hesap süresi',
    },
    title: {
      top: {
        ru: 'Чайка Жуковского',
        en: 'thach\'s',
        tur: 'thach\'s',
      },
      bottom: {
        ru: 'И-153 М-62',
        en: 'f2a-1 buffalo',
        tur: 'f2a-1 buffalo',
      },
    },
  },
  tank: {
    type: 'tank',
    desc: {
      ru: '+3 дня премиум аккаунта',
      en: '+3 day premium account',
      tur: '+3 gün Premium hesap süresi',
    },
    title: {
      top: {
        ru: '(1.ГВ. Т. БР.)',
        en: 'light tank',
        tur: 'light tank',
      },
      bottom: {
        ru: 'Т-26',
        en: 'm2a4',
        tur: 'm2a4',
      },
    },
  },
};

export const FOOTER_LINKS = [
  {
    title: {
      en: 'Privacy Policy',
      tur: 'Privacy Policy',
      ru: 'Политика конфиденциальности',
    },
    url: 'https://warthunder.ru/ru/support/privacypolicy/',
    id: 0,
  },
  {
    title: {
      en: 'EULA',
      tur: 'EULA',
      ru: 'Лицензионное соглашение',
    },
    url: 'https://warthunder.ru/ru/support/eula/',
    id: 1,
  },
  {
    title: {
      en: 'Terms of Use',
      tur: 'Terms of Use',
      ru: 'Условия использования',
    },
    url: 'https://warthunder.ru/ru/support/termsofuse/',
    id: 2,
  },
  {
    title: {
      en: 'Customer Support',
      tur: 'Customer Support',
      ru: 'Поддержка',
    },
    url: 'https://support.gaijin.net/',
    id: 3,
  },
];

export const HEADER = {
  top: {
    en: 'Starter pack',
    ru: 'Набор новичка',
    tur: 'Çaylak Paketi',
  },
  bottom: {
    en: 'for FREE',
    ru: 'бесплатно',
    tur: 'ücretsiz',
  },
};

export const GALLERY = {
  title: {
    top: {
      en: 'video &',
      ru: 'видео и',
      tur: 'video &',
    },
    bottom: {
      en: 'screenshots',
      ru: 'скриншоты',
      tur: 'Ekran Görüntüleri',
    },
  },
};
